'use strict';

var MongoClient = require('mongodb').MongoClient;
var config = require('../config');

/*
 PURPOSE: Creates a single mongodb connection intended to be shared throughout the application.
 */

module.exports = function(callback) {
    var uri = config.mongo.uri;
    MongoClient.connect(uri, { auto_reconnect: true, poolSize: 10 }, function(errors, db) {
        if (!errors) {
            console.log("Connected to MongoDB");
            db.on("close", function() {
                console.log("Closing Connection");
            });
            callback(null, db);
        } else {
            console.log("Error connecting to DB", errors);
            db.close();
            console.log(errors);
            callback("Error connecting to MongoDB");
        }
    });
};