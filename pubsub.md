# pubsub Events

## /initialize
Fires when the application starts up

## /data/restore

indicate that the data should be reloaded

## /view/change, [view]

Fires when the view changes

## /view/changed, [view]

Fires when the view has been changed

## /paddles/save, [name, number]

Fire once form valudation is completed and the paddle data is ready to save

