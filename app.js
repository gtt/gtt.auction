
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var io = require('socket.io');
var _ = require('underscore');
var async = require('async');
var idgen = require('idgen');
var fs = require('fs');
var Faker = require('Faker');
var dataService = require('./services/data-service-mongo');

var app = express();

app.locals.pretty = true;

//indexof mixin http://stackoverflow.com/a/12356923/185045
_.mixin({
    // return the index of the first array element passing a test
    indexOf: function(array, test) {
        // delegate to standard indexOf if the test isn't a function
        if (!_.isFunction(test)) return indexOfValue(array, test);
        // otherwise, look for the index
        for (var x = 0; x < array.length; x++) {
            if (test(array[x])) return x;
        }
        // not found, return fail value
        return -1;
    }
});

// all environments
app.set('port', process.env.PORT || 3010);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('8y34z5onnpt8ivfvdh5wgwq8qch15uh9v5r6okak6xy1r711k7s0mwmz3lij'));
app.use(express.session());
app.use(app.router);
app.use(require('less-middleware')(path.join(__dirname, '/public'), { compress: false }));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

dataService(function(service) {

    var auctions = {};
    var userSessions = {};
    var auctionSessions = {};
    var auctioneers = {};

    app.get('/partials/:name', function(req, res) {
        var p = path.join(__dirname, 'views','_' + req.params.name + ".jade");
        fs.exists(p, function(exists) {
            if (exists) {
                res.render('_' + req.params.name);
            } else {
                res.render("_nothing");
            }
        });

    });

    app.get('/', function(req, res){
        res.render('index', { title: '' });
    });

    app.get('/create-auction/:code', function(req, res) {
        var data = {
            _id: req.params.code,
            serialNumber: 0,
            created: Date.now(),
            guests: [],
            items: [],
            categories: ['Main', 'Silent', 'Kids']
        };

        service.add(data, function(err, result) {
            if (!err) {
                res.send("Auction " + req.params.code + " created");
            } else {
                res.send("Could not create auction at this time");
            }
        });
    });

    var server = http.createServer(app);

    var io = require("socket.io").listen(server, { log: false });

    server.listen(app.get('port'), function() {
        console.log('Express server listening on port ' + app.get('port'));
    });

    io.sockets.on('connection', function (socket) {
        console.log("connecting", socket.id);

        var discon = function(socketId) {
            var auction = userSessions[socketId];
            if (auction) {
                var aus = auctionSessions[auction];
                if (aus) {
                    console.log("attempting to search for user");
                    var id = null;
                    for(var i=0;i<aus.length;i++) {
                        if (aus[i] == socketId) {
                            console.log("Found existing user at position", i);
                            id = i;
                            break;
                        }
                    }
                    if (id != null) {
                        console.log("Removing from auction sessions");
                        auctionSessions[auction].splice(id, 1);
                    }
                    if (auctionSessions[auction].length == 0) {
                        delete auctions[auction];
                        delete auctionSessions[auction];
                    }
                }
            }
            delete userSessions[socketId];
        }

        socket.on('disconnect', function () {
            console.log("disconnecting", socket.id);
            discon(socket.id);
        });

        socket.on('/auctioneer/mode/start', function() {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                auctioneers[auctionName] = auctioneers[auctionName] || [];
                auctioneers[auctionName].push(socket.id);
            }
            console.log(auctioneers[auctionName]);
        });

        socket.on('/auctioneer/mode/end', function() {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                auctioneers[auctionName] = auctioneers[auctionName] || [];

                var socketPosition = auctioneers[auctionName].indexOf(socket.id);
                if(socketPosition !== -1) {
                    auctioneers[auctionName].splice(socketPosition, 1);
                }
            }
            console.log(auctioneers[auctionName]);
        });

        socket.on('/auctioneer/item/selected', function(itemWinner) {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                auctioneers[auctionName] = auctioneers[auctionName] || [];
                console.log("auctioneering", itemWinner);
                _.each(auctioneers[auctionName], function(auctioneerSocketId) {
                    io.sockets.socket(auctioneerSocketId).emit("/auctioneer/item/ondeck", { serial: -1, data: itemWinner });
                });
            }
        });

        socket.on('/auth/destroy', function() {
            console.log("logging off", socket.id);
            discon(socket.id);
        })

        socket.on('/auth/start', function(authData) {
            var code = authData.passcode;
            console.log("/auth/start", code);
            if (!auctions[code]) {
                service.load(code, function(errors, result) {
                    if (!errors && result) {
                        auctions[code] = result;
                        userSessions[socket.id] = code;
                        auctionSessions[code] = auctionSessions[code] || [];
                        auctionSessions[code].push(socket.id);
                        socket.emit("/auth/successful", { serial: -1, data: code });
                    } else {
                        socket.emit('/auth/failure', { serial: -1, data: code });
                    }
                });
            } else {
                userSessions[socket.id] = code;
                auctionSessions[code] = auctionSessions[code] || [];
                auctionSessions[code].push(socket.id);
                socket.emit("/auth/successful", { serial: -1, data: code });
            }
        });

        // development only
        if ('development' == app.get('env')) {
            socket.on('/data/fake', function() {
                console.log('generate fake data');
                var auctionName = userSessions[socket.id];
                var auction = auctions[auctionName];

                if (auction) {
                    auction.guests = [];
                    for(var i=0;i<200;i++) {
                        auction.guests.push({ name: Faker.Name.findName(), number: 1000 + i, id: idgen(8) });
                    }
                    auction.items = [];
                    for(var i=0;i<200;i++) {
                        var categoryRandom = Faker.random.number(9);
                        auction.items.push({
                            number: i + 2,
                            description: Faker.Lorem.sentence(),
                            longDescription: Faker.random.number(3) >= 2 ? Faker.Lorem.sentence() : "",
                            donor: Faker.Name.findName(),
                            id: idgen(8),
                            minimumBid: Faker.random.number(100),
                            maximumBid: Faker.random.number({min: 100, max: 300 }),
                            category: categoryRandom > 6 ? 'Main' : categoryRandom > 3 ? 'Silent' : 'Kids'
                        });
                    }
                }
            });
        }

        socket.on('/data/restore', function(code) {
            console.log("Restoring data", socket.id, code);
            if (!auctions[code]) {

                service.load(code, function(errors, result) {
                    if (!errors && result) {
                        auctions[code] = result;
                        if (!userSessions[socket.id]) {
                            userSessions[socket.id] = code;
                            auctionSessions[code] = auctionSessions[code] || [];
                            auctionSessions[code].push(socket.id);
                        }
                        socket.emit("/data/restored", { serial: -1, data: auctions[code] });
                    } else {
                        console.log("Failure to restore for ", code);
                        socket.emit('/data/restore/failure', { serial: -1, data: code });
                    }
                });
            } else {
                if (!userSessions[socket.id]) {
                    userSessions[socket.id] = code;
                    auctionSessions[code] = auctionSessions[code] || [];
                    auctionSessions[code].push(socket.id);
                }
                socket.emit("/data/restored", { serial: -1, data: auctions[code] });
            }

        });

        socket.on('/paddles/save', function(guest) {
           var g = _.pick(guest, 'name', 'number', 'id');
           if (!g.name && !g.number) {
               socket.emit('/paddles/save/error', { serial: -1, data: g  });
           }
           var newRecord = false;

           if (!g.id) {
               g.id = idgen(8);
               newRecord = true;
           }

            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                g.bids = g.bids || [];
                g.payments = g.payments || [];
                if (newRecord) {
                  auction.guests.push(g);
                }
                for(var i=0;i<auction.guests.length;i++) {
                    if (g.id == auction.guests[i].id) {
                        var payments = auction.guests[i].payments || [];
                        auction.guests[i] = g;
                        auction.guests[i].payments = payments;
                    }
                }
                service.save(auction, function(err, result) {
                    if (!err) {
                        auction.serialNumber++;
                        _.each(auctionSessions[auctionName], function(element, index, list) {
                            io.sockets.socket(element).emit("/paddles/saved", { serial: auction.serialNumber, data: g, originator: element == socket.id });
                        });
                    }
                });
            }
        });

        socket.on('/paddles/remove', function(guestId) {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                var id = null;
                var g = null;
                if (auction && auction.guests) {
                    for(var i=0;i<auction.guests.length;i++) {
                        if (guestId == auction.guests[i].id) {
                            id = i;
                            g = auction.guests[i].id;
                            break;
                        }
                    }
                }

                if (g != null) {
                    if ((g.bids && g.bids.length > 0) || (g.payments && g.payments.length > 0)) {
                        socket.emit("/paddles/remove/error", {serial: -1, data: "Guest has bids" });
                    } else {
                        auction.guests.splice(id, 1);

                        service.save(auction, function(err, result) {
                            if (!err) {
                                auction.serialNumber++;
                                _.each(auctionSessions[auctionName], function(element, index, list) {
                                    io.sockets.socket(element).emit("/paddles/removed", { serial: auction.serialNumber, data: guestId, originator: element == socket.id });
                                });
                            }
                        });
                    }
                }
            }
        });

        socket.on('/items/save', function(auctionitem) {
            var g = _.pick(auctionitem, 'number', 'description', 'longDescription', 'donor', 'minimumBid', 'maximumBid', 'itemValue', 'bidIncrement', 'id', 'category');
            if (!g.number && !g.description && !g.donor && !g.minimumBid > 0 && !g.maximumBid > 0 && !g.category) {
                socket.emit('/items/save/error', { serial: -1, data: g  });
            }
            var newRecord = false;

            if (!g.id) {
                g.id = idgen(8);
                newRecord = true;
            }

            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                if (newRecord) {
                    auction.items.push(g);
                }
                for(var i=0;i<auction.items.length;i++) {
                    if (g.id == auction.items[i].id) {
                        auction.items[i] = g;
                    }
                }

                service.save(auction, function(err, result) {
                    if (!err) {
                        auction.serialNumber++;
                        _.each(auctionSessions[auctionName], function(element, index, list) {
                            io.sockets.socket(element).emit("/items/saved", { serial: auction.serialNumber, data: g, originator: element == socket.id });
                        });
                    }
                });
            }
        });

        socket.on("/bids/combine", function(combineData) {
            console.log("Combining guests");
            var combineGuestId = combineData.combineGuest;
            var combineItemIds = combineData.items || [];
            var auctionName = userSessions[socket.id];
            var items = [];
            if (auctionName && combineItemIds.length > 0) {
                var auction = auctions[auctionName];
                if (auction && auction.items) {
                    _.each(combineItemIds, function(itemId) {
                        var item = _.find(auction.items, function(i) { return i.id == itemId });
                        if (item) {
                            item.bidHistory = item.bidHistory || [];
                            item.bidHistory.push(_.clone(item.bid));
                            item.bid.paddle = combineGuestId;
                            items.push(item);
                        }
                    });
                }
            }
            if (items.length > 0) {
                service.save(auction, function(err, result) {
                    if (!err) {
                        auction.serialNumber++;
                        _.each(auctionSessions[auctionName], function(element, index, list) {
                            io.sockets.socket(element).emit("/bids/combined", { serial: auction.serialNumber, data: items, originator: element == socket.id });
                        });
                    }
                });
            }
        });

        socket.on('/bid/sending', function(bids) {

            var matches = [];

            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                if (auction && auction.items) {
                    for(var i=0;i<auction.items.length;i++) {
                        var item = auction.items[i];
                        var match = bids[item.id];
                        if (match) {
                            item.bid = { paddle: match.paddle, amount: match.amount, created: Date.now() };
                            matches.push(item);
                        }
                    }
                }
                if (matches.length > 0) {
                    service.save(auction, function(err, result) {
                        if (!err) {
                            auction.serialNumber++;
                            _.each(auctionSessions[auctionName], function(element, index, list) {
                                io.sockets.socket(element).emit("/bid/saved", { serial: auction.serialNumber, data: matches, originator: element == socket.id });
                            });
                        }
                    });

                }
            }
        });

        socket.on("/bid/clear", function(itemId) {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                if (auction && auction.items) {
                    var guestId = null;
                    var item = null;
                    for(var i=0;i<auction.items.length;i++) {
                        item = auction.items[i];
                        if(item.id == itemId) {
                            item.bidHistory = item.bidHistory || [];
                            item.bidHistory.push(item.bid);
                            guestId = item.bid.paddle;
                            delete item.bid;
                            break;
                        }
                    }
                    if (item != null && guestId != null) {
                        console.log("Deleting bid for item with id", item.id);
                        service.save(auction, function(err, result) {
                            if (!err) {
                                auction.serialNumber++;
                                _.each(auctionSessions[auctionName], function(element, index, list) {
                                    io.sockets.socket(element).emit("/bid/cleared", { serial: auction.serialNumber, data: { item: item, paddle: guestId }, originator: element == socket.id });
                                });
                            }
                        });

                    }
                }
            }
        });

        socket.on('/checkout/delete', function(paymentDelete) {
            var paymentId = paymentDelete.id;
            var paddleId = paymentDelete.paddle;
            console.log("Attemping to delete", paddleId, paymentId, paymentDelete);

            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                if (auction && auction.guests) {
                    var guest = _.find(auction.guests, function(g) { return g.id == paddleId });
                    console.log("GUEST FOUND", guest);
                    if (guest && guest.payments) {
                        console.log("GUEST HAS PAYMENTS");
                        var deleteId = null;
                        for(var i=0;i<guest.payments.length;i++) {
                            var p = guest.payments[i];
                            if (p.id == paymentId) {
                                console.log("FOUND MATCHING PAYMENT", p);
                                deleteId = i;
                                break;
                            }
                        }
                        if (deleteId != null) {
                            guest.payments.splice(deleteId, 1);
                            service.save(auction, function(err, result) {
                                if (!err) {
                                    auction.serialNumber++;
                                    _.each(auctionSessions[auctionName], function(element, index, list) {
                                        io.sockets.socket(element).emit("/checkout/deleted", { serial: auction.serialNumber, data: paymentDelete, originator: element == socket.id });
                                    });
                                }
                            });
                        }
                    }
                }
            }

        });

        socket.on('/checkout/save', function(payment) {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                if (auction && auction.guests) {
                    var guest = _.find(auction.guests, function(g) { return g.id == payment.paddle });
                    if (guest) {
                        guest.payments = guest.payments || [];
                        var newPayment = {
                            id: idgen(8),
                            amount: payment.amount,
                            checkNumber: payment.checkNumber,
                            method: payment.method,
                            notes: payment.notes,
                            timestamp: Date.now()
                        };
                        guest.payments.push(newPayment);
                        service.save(auction, function(err, result) {
                            if (!err) {
                                newPayment.paddle = guest.id;
                                auction.serialNumber++;
                                _.each(auctionSessions[auctionName], function(element, index, list) {
                                    io.sockets.socket(element).emit("/checkout/saved", { serial: auction.serialNumber, data: newPayment, originator: element == socket.id });
                                });
                            }
                        });
                    }
                }
            }
        });

        socket.on('/items/remove', function(itemId) {
            var auctionName = userSessions[socket.id];
            if (auctionName) {
                var auction = auctions[auctionName];
                var id = null;
                var g = null;
                if (auction && auction.items) {
                    for(var i=0;i<auction.items.length;i++) {
                        if (itemId == auction.items[i].id) {
                            id = i;
                            g = auction.items[i];
                            break;
                        }
                    }
                }

                if (g != null) {
                    if (g.bids && g.bids.length > 0) {
                        socket.emit("/items/remove/error", {serial: -1, data: "Item has bids" });
                    } else {
                        auction.items.splice(id, 1);
                        service.save(auction, function(err, result) {
                            if (!err) {
                                auction.serialNumber++;
                                _.each(auctionSessions[auctionName], function(element, index, list) {
                                    io.sockets.socket(element).emit("/items/removed", { serial: auction.serialNumber, data: itemId, originator: element == socket.id });
                                });
                            }
                        });
                    }
                }
            }
        });

    });
});