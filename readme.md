# Gtt.Auction

This application is an open source web site to manage auction sites for such purposes as charitable fundraisers.

# Planned Functionality

* Authorization & Authentication
* Manage items to be sold in Auction
* Manage patrons attending auction
* Track user payments (offline)
* Reports

# Blog Posts

1. [Introduction](http://gtt.me/code-complete-auction/)
2. [Basic Requirements](http://gtt.me/code-auction-2/)