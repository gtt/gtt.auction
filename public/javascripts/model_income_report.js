var models = models || {};

var IncomeReportModel = function() {

    var model = {
        report: ko.computed(function() {
            var items = models.checkout.paymentList();

            var sortedItems = _.sortBy(items, function(p) {
               return p.guest.name;
            });
            return sortedItems;
        }),

        total: ko.computed(function() {
            var list = models.checkout.paymentList();

            var t = _.reduce(list, function(memo, p) {
                return memo + parseFloat(p.amount);
            }, 0);

            return t;
        }),

        print: function() {
            window.print();
        }
    };

    return model;
};

models.income_report = new IncomeReportModel();

subscribe('/auth/destroy#after', function() {
    log("Clearing income report data");
    models.income_report = new IncomeReportModel();
});
