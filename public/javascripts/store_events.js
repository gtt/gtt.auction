var bidStorage = {
  pendingBids: {},
  code: "",
  timer: null,
  processing: false
};

subscribe('/initialize', function() {
    var auth_code = store.get("auth_code") || null;
    if (auth_code) {
        publish('/auth/start', [{ passcode: auth_code, interactive: false }]);
    } else {
        publish("/auth/new");
    }
});

subscribe('/auth/successful', function(code) {
    log("setting auth token", code);
    store.set('auth_code', code);
    bidStorage.code = code;

    bidStorage.pendingBids = store.get("pendingBids_" + code) || {};
});

subscribe('/items/loaded', function() {
    if (_.keys(bidStorage.pendingBids).length > 0) {
        log('queue items found. Initializing queue');
        publish('/items/initialize/queue', [bidStorage.pendingBids]);
        bidStorage.timer = pendingBidTimerJob();
    }
});

subscribe('/auth/destroy', function() {
    log("Signing out", store.get("auth_code"));
    store.remove('auth_code');
    bidStorage.code = "";
    bidStorage.pendingBids = {};
    bidStorage.processing = false;
    clearInterval(bidStorage.timer);
});

subscribe('/bid/created', function(bid){
    bidStorage.pendingBids[bid.item] = bid;
    store.set("pendingBids_" + bidStorage.code, bidStorage.pendingBids);
    bidStorage.timer = pendingBidTimerJob();
});

subscribe('/bid/sending', function() {
    log("Stopping pending timer due to sending bids");
    clearInterval(bidStorage.timer);
});

subscribe('/bid/saved', function(items) {
    _.each(items, function(item) {
        delete bidStorage.pendingBids[item.id];
        store.set("pendingBids_" + bidStorage.code, bidStorage.pendingBids);
    });

    if (_.keys(bidStorage.pendingBids).length) {
        log("Pending items found. Restarting timer");
        bidStorage.timer = pendingBidTimerJob();
    }
});

var pendingBidTimerJob = function() {
    return setInterval(function() {
        if (!_.isEmpty(bidStorage.pendingBids)) {
            log("Need to save ", _.keys(bidStorage.pendingBids).length, " items");
            if (!bidStorage.processing) {
                publish('/bid/queue', [bidStorage.pendingBids]);
            }
        } else {
            log("stopping timer");
            clearInterval(bidStorage.timer);
        }
    }, 3000);
};