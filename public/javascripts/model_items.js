var models = models || {};

var ItemModel = function() {

    var self = this;
    var eventHandlers = [];

    var model = {
        eventName: "",
        eventDate: "",
        list: ko.observableArray([]),
        printList: ko.observableArray([]),
        filter: ko.observable(''),
        filterCategory: ko.observable(''),
        resultPage: ko.observable(1),
        pageSize: ko.observable(20),
        listSize: ko.observable(0),
        saveMessage: ko.observable(''),
        saving: ko.observable(false),
        showingForm: ko.observable(false),

        categories: ko.observableArray([]),

        print: function(obj, button){
            log("preparing to print");
            model.printList(model.list());
            publish('/items/print',[button]);
        },

        clearFilter: function() {
            model.filter('');
        },
        clearContent: function() {
            model.clearFilter();
        },
        nextPage: function() {
            var c = model.resultPage();
            var max = model.pageSize() * (model.resultPage());
            var size = model.listSize();
            if (max < size) {
                model.resultPage(c + 1);
            }
        },
        prevPage: function() {
            var c = model.resultPage();
            if (c > 1) {
                model.resultPage(c - 1);
            }
        },
        removeItem: function() {
            if (model.form().existingItem.bid) {
                alert("Cannot remove item that has been won by a guest");
                return;
            }

            if (confirm("Are you sure you want to delete " + model.form().description() + "?")) {
                var id = model.form().id();
                log("removing", id);
                publish('/items/remove', [id]);
            }
        },
        editItem: function(itemId) {
            var item = _.find(model.list(), function(g) { return g.id == itemId; });
            if (item) {
                model.form().description(item.description);
                model.form().longDescription(item.longDescription);
                model.form().number(item.number);
                model.form().donor(item.donor);
                model.form().category(item.category);
                model.form().minimumBid(item.minimumBid);
                model.form().id(item.id);
                model.form().existingItem = item;
                model.form().itemValue(item.itemValue);
                model.form().bidIncrement(item.bidIncrement);
                model.form().maximumBid(item.maximumBid);
            }
            model.showForm();
        },
        showForm: function() {
            logger.debug("Showing form");
            model.showingForm(true);
            publish("/ui/modal/start");
        },
        cancelForm: function() {
            logger.debug("Cancelling form");
            model.showingForm(false);
            model.form().reset();
            publish("/ui/modal/end");
        },
        form: ko.validatedObservable({
            description: ko.observable().extend({ required: true }),
            longDescription: ko.observable(),
            number: ko.observable().extend({ required: { params: true, message: "Required" } }),
            donor: ko.observable().extend({ required: true }),
            category: ko.observable().extend({required: true}),
            minimumBid: ko.observable().extend({ required: true, min: 0, number: true }),
            maximumBid: ko.observable().extend({
                required: true,
                min: 0,
                number: true,
                validation: [{
                    validator: function(value) {
                        var min = parseInt(model.form().minimumBid());
                        return !min || (min && min <= parseInt(value));
                    },
                    message: function(params) {
                        return 'Value must be equal or greater than ' + model.form().minimumBid();
                    }
                }]
            }),
            itemValue: ko.observable().extend({ min: .01, number: true }),
            bidIncrement: ko.observable().extend({ min: .5, number: true }),
            id: ko.observable(),
            existingItem: null,
            focusName: ko.observable(false),
            save: function() {
                var f = model.form();
                var existingItem = _.find(model.list(), function(item) {  return item.number == f.number() && item.id != f.id(); });
                if (existingItem) {
                    f.number.setError("Number already used");
                }

                if (model.form.isValid()) {
                    publish('/items/save', [{
                        description: f.description(),
                        longDescription: f.longDescription(),
                        number: f.number(),
                        donor: f.donor(),
                        category: f.category(),
                        minimumBid: f.minimumBid(),
                        maximumBid: f.maximumBid(),
                        itemValue: f.itemValue(),
                        bidIncrement: f.bidIncrement(),
                        id: f.id()
                    }]);
                    model.saving(true);
                } else {
                    model.form.errors.showAllMessages();
                }
            },
            reset: function() {
                var f = model.form;
                f().description('');
                f().description.isModified(false);
                f().longDescription('');
                f().longDescription.isModified(false);
                f().number('');
                f().number.isModified(false);
                f().donor('');
                f().donor.isModified(false);
                f().category('');
                f().category.isModified(false);
                f().minimumBid('');
                f().minimumBid.isModified(false);
                f().maximumBid('');
                f().maximumBid.isModified(false);
                f().itemValue('');
                f().itemValue.isModified(false);
                f().bidIncrement('');
                f().bidIncrement.isModified(false);
                f().id('');
                f().existingItem = null;
            },
            new: function() {
                log("setting item form focus");
                setTimeout(function() {
                    model.form().focusName(true);
                }, 300);
            }
        })
    };

    model.filter.subscribe(function(newValue) {
        model.resultPage(1);
    });

    model.filteredList = ko.computed(function() {
        var filter = model.filter();
        var filterCategory = model.filterCategory();
        var page = model.resultPage();
        var pageSize = model.pageSize();
        var offset = (page - 1) * pageSize;
        var sortedList = _.sortBy(model.list(), function(g) { return g.number; });

        var re = new RegExp(filter,"gi");
        var filteredList = ko.utils.arrayFilter(sortedList, function(item) {
            item.itemValue = item.itemValue || null;
            item.bidIncrement = item.bidIncrement || null;
            var matchCategory = !filterCategory || filterCategory == item.category;
            var textMatch = !filter || (item.description + "|" + item.donor + "|" + item.number + "|" + item.longDescription).match(re);
            return matchCategory && textMatch;
        });
        var list = _.chain(filteredList).rest(offset).first(pageSize).value();
        model.listSize(filteredList.length);
        return list;
    });

    model.pageMin = ko.computed(function() {
        return (model.pageSize() * model.resultPage()) - model.pageSize() + 1
    });

    model.pageMax = ko.computed(function() {
        var c = (model.pageSize() * model.resultPage());
        var m = model.listSize();
        return m > c ? c : m;
    });

    model.clearEventHandlers = function() {
        for(var i=0;i<eventHandlers.length;i++) {
            unsubscribe(eventHandlers[i]);
        }
    };

    eventHandlers.push(subscribe('/items/saved', function(item, originator, suppressMessage) {
        var id = null;
        var list = model.list();
        for(var i=0;i<list.length;i++) {
            if (item.id == list[i].id) {
                id = i;
            }
        }

        item.itemValue = item.itemValue || null;
        item.bidIncrement = item.bidIncrement || null;

        if (id != null) {
            model.list.splice(id, 1, item);
        } else {
            model.list.push(item);
        }

        if (originator) {
            model.cancelForm();
            publish("/ui/notifications/flash", ['Auction item updated']);

        }

    }));

    eventHandlers.push(subscribe('/items/save/error', function(guest) {
        model.form().number.setError("Number already used");
    }));

    eventHandlers.push(subscribe('/items/printed', function() {
        model.printList([]);
    }));


    eventHandlers.push(
        subscribe('/items/removed', function(itemId, originator) {
            if (originator) {
                var f = model.form().reset();
            }
            var id = null;
            var list = model.list();
            for(var i=0;i<list.length;i++) {
                if (itemId == list[i].id) {
                    id = i;
                    break;
                }
            }
            if (id != null) {
                model.list.splice(id,1);
            }
            if (originator) {
                model.cancelForm();
                publish('/ui/notifications/flash', ['Item removed']);
            }
        })
    );

    eventHandlers.push(subscribe('/data/restored', function(data) {
        logger.debug("restoring categories");
        if (data.categories) {
            model.categories(data.categories);
        }

        logger.debug("restoring item data");

        _.each(data.items, function(item) {
           item.itemValue = item.itemValue || null;
           item.bidIncrement = item.bidIncrement || null;
        });
        model.list(data.items);
        model.eventName = data.name || "Auction Event";
        var d = data.eventDate || new Date();
        model.eventDate = moment(d).format("MM/DD/YYYY");
        setTimeout(function() {
            publish('/items/loaded');
        }, 0);
    }));

    return model;
}

models.items = new ItemModel();

subscribe('/view/changed', function() {
    models.items.clearContent();
});

subscribe('/auth/destroy', function() {
    log("Clearing item data");
    models.items.clearEventHandlers();
    models.items = new ItemModel();
});