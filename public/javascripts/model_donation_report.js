var models = models || {};

var DonationReportModel = function() {

    var eventHandlers = [];

    var dummy = ko.observable();

    var model = {
        eventName: "",
        sort: ko.observable("itemNumber"),
        grouping: ko.observable("nogroup"),
        showBidRange: ko.observable("hide"),
        showWinners: ko.observable("hide"),
        report: ko.computed(function() {
            dummy();
            var items = models.items.list();
            var sortedList = _.sortBy(items, function(item) {

                if (model.grouping() == "category") {
                    return item.category + "" + item[model.sort()];
                } else {
                    return item[model.sort()];
                }

            });
            return sortedList;
        }),

        print: function() {
            window.print();
        }
    };

    model.sort.subscribe(function() {
        dummy.notifySubscribers();
    });

    model.grouping.subscribe(function() {
        dummy.notifySubscribers();
    });

    model.clearEventHandlers = function() {
        for(var i=0;i<eventHandlers.length;i++) {
            unsubscribe(eventHandlers[i]);
        }
    };

    eventHandlers.push(subscribe('/data/restored', function(data) {
        model.eventName = data.name;
    }));

    return model;
};

models.donation_report = new DonationReportModel();

subscribe('/auth/destroy', function() {
    models.donation_report.clearEventHandlers();
});

subscribe('/auth/destroy#after', function() {
    log("Clearing donation report data");
    models.donation_report = new DonationReportModel();
});
