subscribe("/app/ready", function() {
    publish("/remote/connecting");
    var socket = io.connect();

    var socketApi = {
        registered: false,
        serialNumber: 0,
        inbound: [
            {
                msg: '/auth/successful',
                sync: false
            },
            {
                msg: '/auth/failure',
                sync: false
            },
            {
                msg: '/data/restored',
                sync: false
            },
            {
                msg: '/data/restore/failure',
                sync: false
            },
            {
                msg: '/paddles/saved',
                sync: true
            },
            {
                msg: '/paddles/remove/error',
                sync: true
            },
            {
                msg: '/paddles/removed',
                sync: true
            },
            {
                msg: '/items/saved',
                sync: true
            },
            {
                msg: '/items/save/error',
                sync: false
            },
            {
                msg: '/items/remove/error',
                sync: true
            },
            {
                msg: '/items/removed',
                sync: true
            },
            {
                msg: '/bid/saved',
                sync: true
            },
            {
                msg: '/bid/cleared',
                sync: true
            },
            {
                msg: '/bids/combined',
                sync: true
            },
            {
                msg: '/checkout/saved',
                sync: true
            },
            {
                msg: '/checkout/deleted',
                sync: true
            },
            {
                msg: '/auctioneer/item/ondeck',
                sync: false
            }
        ],
        outbound: [
            '/auth/start',
            '/auth/destroy',
            '/paddles/save',
            '/paddles/remove',
            '/items/save',
            '/items/remove',
            '/data/restore',
            '/data/fake',
            '/bid/sending',
            '/bid/clear',
            '/bids/combine',
            '/checkout/save',
            '/checkout/delete',
            '/item/selected',
            '/auctioneer/mode/start',
            '/auctioneer/mode/end',
            '/auctioneer/item/selected'
        ]
    };

    function registerInbound(socket, inboundMessage) {
        var message = inboundMessage.msg;
        var sync = inboundMessage.sync;
        logger.info("Registering Inbound", message);
        socket.on(message, function(response) {
            if (sync) {
                socketApi.serialNumber++;
                logger.info("Expecting Serial Number=", socketApi.serialNumber, " Received=", response.serial);
                if (response.serial >= 0 && socketApi.serialNumber != response.serial) {
                    logger.warn("Data resync needed");
                    publish("/data/resync");
                }
            }
            if (message == "/data/restored") {
                logger.debug("updating serial number to", response.data.serialNumber);
                socketApi.serialNumber = response.data.serialNumber;
            }
            logger.debug("Socket - Receiving", message, response);
            publish(message, [response.data, response.originator]);
        });
    }

    function registerOutbound(socket, message) {
        logger.info("Registering outbound", message);
        subscribe(message, function(data) {
            if (socket.socket.connected) {
                logger.debug("Socket - Sending", message, data);
                socket.emit(message, data);
            } else {
              loger.error("cannot emit", message, "socket is disconnected");
            }
        });
    }

    socket.on("connect", function(){
        logger.info("Connecting", socket.socket.sessionid);
        publish('/remote/connected');

        if (!socketApi.registered) {
            for(var i=0;i<socketApi.inbound.length;i++){
                registerInbound(socket, socketApi.inbound[i]);
            }

            for(var i=0;i<socketApi.outbound.length;i++){
                registerOutbound(socket, socketApi.outbound[i]);
            }
        }

        socketApi.registered = true;

        publish("/initialize");

    });

    socket.on('disconnect', function(){
        logger.error("Disconnected from sockets");
        socketApi.serialNumber = 0;
        publish('/remote/disconnected');
    });

    socket.on('reconnect_failed', function() {
        logger.error("Reconnection attempt failed");
    });

    socket.on('connect_failed', function() {
        logger.debug("Connect failed");
    });

    subscribe('/bid/queue', function(items) {
        if(socket.socket.connected) {
            publish('/bid/sending', [items]);
        }
    });

    subscribe('/auth/destroy', function() {
        logger.warn("Resetting socket serial number");
        socketApi.serialNumber = 0;
    });
});