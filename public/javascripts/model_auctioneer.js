var models = models || {};

var AuctioneerModel = function() {
    var self = this;
    var eventHandlers = [];

    var model = {
        itemWinner: null,
        is_auctioneer: false,

        item: {
            present: ko.observable(false),
            number: ko.observable(""),
            description: ko.observable(""),
            longDescription: ko.observable(""),
            donor: ko.observable(""),
            minBid: ko.observable(0),
            maxBid: ko.observable(1000)
        },

        guest: {
            present: ko.observable(false),
            name: ko.observable(""),
            number: ko.observable("")
        }
    };

    model.clearEventHandlers = function() {
        for(var i=0;i<eventHandlers.length;i++) {
            unsubscribe(eventHandlers[i]);
        }
    };

    eventHandlers.push(subscribe("/auctioneer/item/ondeck", function(itemWinner) {
        if (itemWinner.item != null) {
            model.item.number(itemWinner.item.number);
            model.item.description(itemWinner.item.description);
            model.item.longDescription(itemWinner.item.longDescription);
            model.item.donor(itemWinner.item.donor);
            model.item.minBid(itemWinner.item.minimumBid);
            model.item.maxBid(itemWinner.item.maximumBid);
            model.item.present(true);
        } else {
            model.item.number("");
            model.item.description("");
            model.item.longDescription("");
            model.item.donor("");
            model.item.minBid("");
            model.item.maxBid("");
            model.item.present(false);
        }

        if (itemWinner.guest != null) {
            model.guest.present(true);
            model.guest.name(itemWinner.guest.name);
            model.guest.number(itemWinner.guest.number);
        } else {
            model.guest.present(false);
            model.guest.name("");
            model.guest.number("");
        }
    }));

    return model;
};

models.auctioneer = new AuctioneerModel();

subscribe('/view/changed', function(view) {

    if (view != "auctioneer" && models.auctioneer.is_auctioneer) {
        logger.info("Exiting auctioneer mode");
        publish('/auctioneer/mode/end');
        models.auctioneer.is_auctioneer = false;
    }

    if (view == "auctioneer") {
        logger.info("Entering auctioneer mode");
        publish('/auctioneer/mode/start');
        models.auctioneer.is_auctioneer = true;
    }
});

subscribe('/auth/destroy', function() {
    log("Clearing auctioneer data");
    publish('/auctioneer/mode/end');
    models.auctioneer.clearEventHandlers();
    models.auctioneer = new AuctioneerModel();
});