function PagingControl(originalList) {

    var model = {
        list: ko.isObservableArray(originalList) ? originalList : ko.observerableArray(originalList),
        filter: ko.observable(''),
        resultPage: ko.observable(1),
        pageSize: ko.observable(20),
        listSize: ko.observable(0)
    };


    model.filteredList = ko.computed(function() {
        var filter = model.filter();
        var page = model.resultPage();
        var pageSize = model.pageSize();
        var offset = (page - 1) * pageSize;
        var sortedList = _.sortBy(model.list(), function(g) { return g.number; });
        if (!filter) {
            var pagedOriginalList = _.chain(sortedList).rest(offset).first(pageSize).value();
            model.listSize(sortedList.length);
            return pagedOriginalList;
        }

        var re = new RegExp(filter, "gi");
        var filteredList = ko.utils.arrayFilter(sortedList, function(item) {
            return (item.description + "|" + item.donor + "|" + item.number + "|" + item.longDescription).match(re);
        });
        var pagedFilteredList = _.chain(filteredList).rest(offset).first(pageSize).value();
        model.listSize(filteredList.length);
        return pagedFilteredList;
    });

    return model;
}