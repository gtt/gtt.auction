var Logger = function(opts) {
    opts = opts || {};

    var log_level_val = function(name) {
        if (name == "TRACE") return 1;
        if (name == "DEBUG") return 2;
        if (name == "INFO") return 3;
        if (name == "WARN") return 4;
        if (name == "ERROR") return 5;
        if (name == "FATAL") return 6;
        return 0;
    };

    var logMessages = (/logging/).test(window.location.search) || opts.enabled || false;
    var logToUserInterface = opts.ui || true;
    var logToConsole = opts.console || true;
    var qs_arr = window.location.search.match(/logging=(.*?)([#=]|$)/);
    var qs_level = "DEBUG";
    if (qs_arr) {
        qs_level = qs_arr[1].toUpperCase();
    }
    var level_score = log_level_val(qs_level || opts.level);
    var height = opts.height || 100;
    var afterLog = opts.after;
    var resizable = opts.resizable || true;

    if (!level_score) {
        console.log("WARN", "Cannot interprete log level=" + opts.level +'. Using DEBUG');
        level_score = log_level_val("DEBUG");
    }

    if (opts.init && typeof(opts.init) == "function") {
        opts.init();
    }

    var createButton = function(id, text, fromRight, topOffset, onclick) {
      return '<button id="' + id + '" style="' +
          'padding: 2px; height: 1.3em; line-height: 1em; ' +
          'position: fixed; font-family: arial; font-size: 12px; ' +
          'bottom: ' + (height - topOffset) + '; ' +
          'background-color: #fff; border: 1px solid #ddd; ' +
          'min-width: 17px; display: inline-block;' +
          'right: ' + fromRight + 'px; color: #333" ' +
          'onclick="'+ onclick + '">' +
          text + '</button>';
    };

    var clearBtn = createButton('', 'clear', 41, 12, 'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');
    var pauseBtn = createButton('', 'p', 23, 12, 'document.getElementById(\'gtt-console\').style.display = \'none\'');
    var closeBtn = createButton('gtt-logger-close', 'X', 5, 12, 'document.getElementById(\'gtt-console\').style.display = \'none\'');

    var trace = createButton('gtt-logger-trace', 'T', 96, 0,'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');
    var debug = createButton('gtt-logger-debug', 'D', 78, 0,'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');
    var info = createButton('gtt-logger-info', 'I', 60, 24,'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');
    var warn = createButton('gtt-logger-warn', 'W', 41, 24,'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');
    var error = createButton('gtt-logger-error', 'E',  23, 24,'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');
    var fatal = createButton('gtt-logger-fatal', 'F', 5, 24,'document.getElementById(\'gtt-console-list\').innerHTML = \'\'');

    var consolePanel = '<div id="gtt-console" style="opacity: .8; position: fixed; bottom: 0; left: 0; right: 0; width: 100%; height: ' + height + 'px; background-color: #333; color: #FFF; overflow: scroll; font-family: courier, monospace; font-size: 12px; padding: 5px;">' + clearBtn + pauseBtn + closeBtn + '<br />' + trace + debug + info + warn + error + fatal + '<ol style="margin: 0; padding: 0;" id="gtt-console-list" reversed="reversed"></ol></div>';

    if (logMessages && logToUserInterface) {
        var body = document.body;
        body.style.paddingBottom = height + 'px';
        body.innerHTML = body.innerHTML + consolePanel;

        document.getElementById("gtt-logger-close").addEventListener("click", function() {
            logMessages = false;
        });
        document.getElementById("gtt-logger-trace").addEventListener("click", function() {
            level_score = log_level_val("TRACE");
            log('MSG', ['Logging TRACE']);
        });
        document.getElementById("gtt-logger-debug").addEventListener("click", function() {
            level_score = log_level_val("DEBUG");
            log('MSG', ['Logging DEBUG']);
        });
        document.getElementById("gtt-logger-info").addEventListener("click", function() {
            level_score = log_level_val("INFO");
            log('MSG', ['Logging INFO']);
        });
        document.getElementById("gtt-logger-warn").addEventListener("click", function() {
            level_score = log_level_val("WARN");
            log('MSG', ['Logging WARN']);
        });
        document.getElementById("gtt-logger-error").addEventListener("click", function() {
            level_score = log_level_val("ERROR");
            log('MSG', ['Logging ERROR']);
        });
        document.getElementById("gtt-logger-fatal").addEventListener("click", function() {
            level_score = log_level_val("FATAL");
            log('MSG', ['Logging FATAL']);
        });

        if (resizable) {
            var windowHeight = window.innerHeight;

            var down = false;
            var y_start = 0;
            var y_end = 0;

            window.addEventListener("resize", function () {
                windowHeight = window.innerHeight;
            });

            body.addEventListener('mousedown', function (e) {
                y_start = e.clientY || e.pageY;
                var startPoint = windowHeight - y_start;
                if (startPoint <= height + 10 && startPoint >= height - 10) {
                    down = true;
                }
            });
            body.addEventListener('mousemove', function (e) {
                var s = e.clientY || e.pageY;

                var startPoint = windowHeight - s;
                if (startPoint <= height + 10 && startPoint >= height - 10) {
                    body.style.cursor = "ns-resize";
                } else {
                    body.style.cursor = "default";
                }

                if (down) {
                    var distance = s - y_start;
                    var h = height - distance;
                    if (Math.abs(distance) > 3) {
                        document.getElementById("gtt-console").style.height = h + "px";
                        body.style.cursor = "ns-resize";
                    }
                    //pauseEvent(e);
                }
            });
            body.addEventListener('mouseup', function (e) {
                y_end = e.clientY || e.pageY;
                if (down) {
                    var distance = y_end - y_start;
                    if (Math.abs(distance) > 3) {
                        height = height - distance;
                        document.getElementById("gtt-console").style.height = height + "px";
                        body.style.cursor = "ns-resize";
                    }
                }
                down = false;
            });
        }
    }

    var pauseEvent = function(e) {
        if(e.stopPropagation) e.stopPropagation();
        if(e.preventDefault) e.preventDefault();
        e.cancelBubble=true;
        e.returnValue=false;
        return false;
    };

    var log = function(name, args) {
        if (logMessages && (name == 'MSG' || log_level_val(name) >= level_score)) {

            document.getElementById("gtt-console").style.display = "block";

            if (args) {
                var arr = Array.prototype.slice.call(args);
                for(var i=0;i<arr.length;i++) {
                    if (arr[i]) {
                        var str = JSON.stringify(arr[i]);
                        if (str) {
                            arr[i] = str;
                        }
                    }
                }
                if (logToUserInterface) {
                    var msg = arr.join(" ");
                    var entry = '<li class="console console-' + name.toLowerCase() + '" style="margin: 0; padding: 0"><span class="console-category">' + name + '</span> ' + msg + '</li>';
                    var list = document.getElementById("gtt-console-list");
                    list.innerHTML = entry + list.innerHTML;
                }
                if (logToConsole) {
                    arr.unshift(name);
                    console.log.apply(console, arr);
                }
                if (typeof(afterLog) == "function") {
                    afterLog(name, args);
                }
            }
        }
    };

    return {
        trace: function() { log("TRACE", arguments); },
        debug: function() { log("DEBUG", arguments); },
        info:  function() { log("INFO",  arguments); },
        warn:  function() { log("WARN",  arguments); },
        error: function() { log("ERROR", arguments); },
        fatal: function() { log("FATAL", arguments); },
        activate: function() {
            logMessages = true;
        }
    };
};

var logger = new Logger({ level: "DEBUG" });

// wrap default console.log
var log = function() {
    logger.debug.apply(log, arguments);
};
