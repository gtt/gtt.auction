var models = models || {};

models.login = {

    form: ko.validatedObservable({
        passcode: ko.observable('').extend({ required: true }),
        auth: function() {
            publish('/auth/start', [{ passcode: models.login.form().passcode(), interactive: true }]);
        }
    })

};

subscribe('/auth/failure', function() {
    logger.warn("Passcode not recognized")
    models.login.form().passcode.setError("Passcode not recognized");
});

subscribe('/auth/successful', function(data) {
    models.login.form().passcode('');
    models.login.form().passcode.isModified(false);
});