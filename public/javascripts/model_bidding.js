var models = models || {};

var BiddingModel = function() {
    var self = this;
    var eventHandlers = [];

    var publishSelectionsToAuctioneer = function() {
        if (model.isAuctioneerAssistant()) {
            publish('/auctioneer/item/selected', [{ item: model.form().item, guest: model.form().guest == null ? null : { name: model.form().guest.name, number: model.form().guest.number } }]);
        }
    };

    var clearSelectionsToAuctioneer = function() {
        publish('/auctioneer/item/selected', [{ item: null, guest: null }]);
    };

    var model = {
        bidItems: ko.observableArray([]),
        isAuctioneerAssistant: ko.observable(false),
        selectGuest: function(guest) {
            model.form().guestName(guest.name + " (" + guest.number + ")");
            model.form().guest = guest;

            publishSelectionsToAuctioneer();

            return false;
        },

        selectItem: function(item) {
            model.form().itemName(item.description + " (" + item.number + ")");
            model.form().item = item;

            var min = item.minimumBid || 0;
            var max = item.maximumBid || 1000;

            model.form().range(accounting.formatMoney(min) + ' - ' + accounting.formatMoney(max));

            publishSelectionsToAuctioneer();

            return false;
        },

        pendingSaves: ko.observable(0),

        editBid: function(item) {
            model.editForm().item = item;
        },

        deleteBid: function(item) {
            if (confirm("Are you sure you want to clear the bid for " + item.description)) {
                publish("/ui/waiting/start");
                publish("/bid/clear", [item.id]);

            }
        },

        editForm: ko.validatedObservable({
            item: null,

            delete: function() {
                var itemToClear = model.editForm().item;
                if (confirm("Are you sure you want to clear the bid for " + itemToClear.description)) {
                    publish("/bid/clear", [itemToClear.id]);
                }
            }
        }),
        form: ko.validatedObservable({
            guestLookup: ko.observable(""),
            guestList: ko.observableArray([]),
            itemLookup: ko.observable(""),
            itemList: ko.observableArray([]),
            bid: ko.observable().extend({ number: true, required: { params: true, message: "Required" } }),
            guestName: ko.observable(""),
            itemName: ko.observable(""),
            show_override: ko.observable(false),
            override: ko.observable(false),
            guest: null,
            item: null,
            range: ko.observable(""),
            clearGuest: function() {
                var g = model.form().guest;
                model.form().guest = null;
                model.form().guestName('');
                model.form().guestLookup('');
                model.form().guestName.isModified(false);
                publishSelectionsToAuctioneer();
            },
            clearItem: function() {
                var g = model.form().item;
                model.form().item = null;
                model.form().itemName('');
                model.form().itemLookup('');
                model.form().itemName.isModified(false);
                model.form().range('');
                publishSelectionsToAuctioneer();
            },
            clearForm: function() {
                model.form().clearGuest();
                model.form().clearItem();
                model.form().bid('');
                model.form().bid.isModified(false);
                model.form().show_override(false);
                model.form().override(false);
                publishSelectionsToAuctioneer();
            },

            save_with_override: function() {
                logger.debug("save with override");
                model.form().override(true);
                model.form().save();
            },

            save: function() {
                model.form().bid.isModified(false);

                logger.debug("saving bid");
                var f = model.form();
                if (!f.guest) {
                    f.guestName.setError("Please select a guest");
                }
                if(!f.item) {
                    f.itemName.setError("Please select an item");
                }

                var bid_amount = parseFloat(f.bid());
                var override = f.override();
                var maxBidFailure = !override && bid_amount && f.item && f.item.maximumBid && f.item.maximumBid > 0 && bid_amount > f.item.maximumBid;
                var minBidFailure = !override && bid_amount && f.item && f.item.minimumBid && f.item.minimumBid > 0 && bid_amount < f.item.minimumBid;

                if (!bid_amount) {
                    logger.debug("no bid amount present");
                    f.bid.setError("Required");
                } else if (minBidFailure || maxBidFailure){
                    logger.debug({msg: "setting min/max error", amount: bid_amount, min: minBidFailure, max: maxBidFailure });
                    f.bid.setError("Bid amount must be between " + accounting.formatMoney(f.item.minimumBid) + " and " + accounting.formatMoney(f.item.maximumBid))
                    model.form().show_override(true);
                } else {
                    f.bid.clearError();
                }

                var valid = model.form.isValid();
                if (valid) {
                    publish('/bid/created', [{ item: f.item.id, paddle: f.guest.id, amount: f.bid() }]);
                    var list = models.items.list();
                    f.item.pending = true;
                    model.form().clearForm();
                    var saves = model.pendingSaves();
                    saves++;
                    model.pendingSaves(saves);
                } else {
                    model.form.errors.showAllMessages();
                }
            }

        })
    };

    model.form().guestLookup.subscribe(function(val) {
        if (!val) {
            model.form().guestList([]);
            return;
        }
        var guests = models.paddles.list();
        var re = new RegExp(val,"gi");
        var direct = null;
        var count = 0;
        var filteredList = ko.utils.arrayFilter(guests, function(item) {
            if (item.number == val) {
                direct = item;
                count++;
                return false;
            }
            if (count == 5 || item.bid) {
                return false;
            }
            var m = (item.name + " " + item.number).match(re);
            if (m) {
                count++;
            }
            return m;
        });

        if (direct) {
            filteredList.unshift(direct);
        }
        model.form().guestList(filteredList);
    });

    model.form().itemLookup.subscribe(function(val) {
        if (!val) {
            model.form().itemList([]);
            return;
        }
        var items = models.items.list();
        var re = new RegExp(val,"gi");
        var direct = null;
        var count = 0;
        var filteredList = ko.utils.arrayFilter(items, function(item) {
            if (item.pending || item.bid) {
                return false;
            }
            if (item.number == val) {
                direct = item;
                count++;
                return false;
            }
            if (count == 5) {
                return false;
            }
            var m = (item.description + " " + item.number).match(re);
            if (m) {
                count++;
            }
            return m;
        });

        if (direct) {
            filteredList.unshift(direct);
        }
        model.form().itemList(filteredList);
    });

    model.isAuctioneerAssistant.subscribe(function(val) {
        if (val) {
            publishSelectionsToAuctioneer();
        } else {
            clearSelectionsToAuctioneer();
        }
    });

    model.clearEventHandlers = function() {
        for(var i=0;i<eventHandlers.length;i++) {
            unsubscribe(eventHandlers[i]);
        }
    };

    eventHandlers.push(subscribe('/data/restored', function(data) {
        setTimeout(function() {
            log("restoring bidding data");

            var guests = _.object(_.map(data.guests, function(item) {
                return [item.id, item];
            }));

            var items = data.items;
            if (items) {
                for(var i=0;i<items.length;i++) {
                    if (items[i].bid) {
                        var guest = guests[items[i].bid.paddle];
                        items[i].bid.guest = guest;
                        guest.bids = guest.bids || [];
                        guest.bids.push(items[i]);
                        model.bidItems.push(items[i])
                    }
                }
            }
            publish("/bid/changed");
        }, 0);
    }));

    eventHandlers.push(subscribe('/bid/saved', function(items, originator) {
        if (originator) {
            var itemCount = _.keys(items).length;
            var current = model.pendingSaves();
            model.pendingSaves(current - itemCount);
        }
        _.each(items, function(item) {
            var suppressMessage = true;
            publish('/items/saved', [item, originator, suppressMessage]);
            guest = _.find(models.paddles.list(), function(g) { return g.id == item.bid.paddle });
            guest.bids = guest.bids || [];
            guest.bids.push(item);
            item.bid.guest = guest;
            model.bidItems.unshift(item);
        });
        publish("/bid/changed");
    }));

    eventHandlers.push(subscribe('/bid/cleared', function(itemAndPaddleId, originator) {
        var suppressMessage = true;
        publish('/items/saved', [itemAndPaddleId.item, originator, suppressMessage]);
        guest = _.find(models.paddles.list(), function(g) { return g.id == itemAndPaddleId.paddle });

        var guestBidPosition = _.indexOf(guest.bids, function(gb) { return gb.id == itemAndPaddleId.item.id });
        guest.bids.splice(guestBidPosition, 1);

        var bidPosition = _.indexOf(model.bidItems(), function(it) { return it.id == itemAndPaddleId.item.id; });
        model.bidItems.splice(bidPosition, 1);
        publish("/bid/changed");
        publish("/ui/waiting/end");
    }));

    eventHandlers.push(subscribe('/items/initialize/queue', function(queue) {
        var list = models.items.list();
        var c = 0;
        for(var i=0;i<list.length;i++) {
            c++;
            var item = list[i];
            var match = queue[item.id];
            if (match) {
                item.pending = true;
            }
        }
        model.pendingSaves(queue.length);
        log("Connected queue in", c, "iterations");
    }));

    return model;
};

models.bidding = new BiddingModel();

subscribe('/view/changed', function() {
    models.bidding.isAuctioneerAssistant(false);
    models.bidding.form().clearForm();
});

subscribe('/auth/destroy', function() {
    log("Clearing bidding data");
    models.bidding.clearEventHandlers();
    models.bidding = new BiddingModel();
});