var models = models || {};

var GuestReportModel = function() {

    var model = {
        report: ko.computed(function() {
            var items = models.paddles.list();

            var filteredList = [];



            if (model && model.outstandingPaymentsOnly()) {
                filteredList = _.filter(items, function(i) {
                    i.expectedPayment = i.expectedPayment || 0;
                    i.totalPayments = i.totalPayments || 0;
                    return i.expectedPayment > i.totalPayments;
                });
            } else {
                filteredList = _.each(items, function(i) {
                    i.expectedPayment = i.expectedPayment || 0;
                    i.totalPayments = i.totalPayments || 0;
                });
            }


            var sortedList = _.sortBy(filteredList, function(i2) {
                return i2.name;
            });

            return sortedList;
        }),

        outstandingPaymentsOnly: ko.observable(false),

        print: function() {
            window.print();
        }
    };

    return model;
};

models.guest_report = new GuestReportModel();

subscribe('/auth/destroy#after', function() {
    log("Clearing guest report data");
    models.guest_report = new GuestReportModel();
});