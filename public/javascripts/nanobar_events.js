var nanobar = new Nanobar({
    bg: "#306e73",
    id: "progressbar",
    target: document.getElementById("progressbarHolder")
});

subscribe('/items/save', function() {
    nanobar.go(30);
});

subscribe('/items/saved', function() {
    nanobar.go(100);
});

subscribe('/items/removed', function() {
    nanobar.go(100);
});