var models = models || {};

var CheckReportModel = function() {

    var model = {

        showNames: ko.observable(true),

        report: ko.computed(function() {
            var payments = models.checkout.paymentList();
            var checks = _.filter(payments, function(p) { return p.method == "check"; });
            return checks;
        }),

        print: function() {
            window.print();
        }
    };


    return model;
};

models.check_report = new CheckReportModel();

subscribe('/auth/destroy#after', function() {
    log("Clearing check report data");
    models.check_report = new CheckReportModel();
});
