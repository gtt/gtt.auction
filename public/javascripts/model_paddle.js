var models = models || {};

var PaddleModel = function() {
    var self = this;
    var eventHandlers = [];

    var model =  {
        list: ko.observableArray([]),
        filter: ko.observable(''),
        resultPage: ko.observable(1),
        pageSize: ko.observable(20),
        listSize: ko.observable(0),
        editingGuest: null,
        showingForm: ko.observable(false),

        clearFilter: function() {
            model.filter('');
        },

        clearContent: function() {
            model.clearFilter();
        },

        nextPage: function() {
            var c = model.resultPage();
            var max = model.pageSize() * (model.resultPage());
            var size = model.listSize();
            if (max < size) {
                model.resultPage(c + 1);
            }
        },

        prevPage: function() {
            var c = model.resultPage();
            if (c > 1) {
                model.resultPage(c - 1);
            }
        },

        showForm: function() {
            logger.debug("Showing form");
            model.showingForm(true);
            publish("/ui/modal/start");
        },

        cancelForm: function() {
            logger.debug("Cancelling form");
            model.showingForm(false);
            model.form().reset();
            publish("/ui/modal/end");
        },

        editGuest: function(item) {
            var guest = _.find(model.list(), function(g) { return g.id == item; });
            if (guest) {
                model.form().name(guest.name);
                model.form().number(guest.number);
                model.form().id(guest.id);
                model.form().editingGuest = guest;
            }
            model.showingForm(true);
            publish("/ui/modal/start");
        },
        removeGuest: function() {
            var guest = model.form().editingGuest;
            if (guest.bids && guest.bids.length > 0) {
                alert("Cannot delete guest who has won items");
                return;
            }
            if (confirm("Are you sure you want to delete " + model.form().name() + "?")) {
                var id = model.form().id();
                log("removing", id);
                publish('/paddles/remove', [id]);
            }
        },

        form: ko.validatedObservable({
            name: ko.observable().extend({ required: true }),
            number: ko.observable().extend({ required: true }),
            id: ko.observable(),
            focusName: ko.observable(false),

            save: function() {
                logger.debug("attempting save");
                var f = model.form();

                var existingPaddle = _.find(this.list(), function(item) { return item.number == f.number() && item.id != f.id(); });
                if (existingPaddle) {
                    f.number.setError("Paddle number already in use by " + existingPaddle.name);
                }

                if (model.form.isValid()) {
                    publish('/paddles/save', [{ name: f.name(), number: f.number(), id: f.id() }]);
                } else {
                    logger.debug("Paddle model is invalid", f);
                    model.form.errors.showAllMessages();
                }
            },

            reset: function() {
                model.form().name('');
                model.form().name.isModified(false);
                model.form().number('');
                model.form().number.isModified(false);
                model.form().id('');
                model.form().existingGuest = null;
            },

            new: function() {
                log("setting paddle form focus");
                setTimeout(function() {
                    model.form().focusName(true);
                }, 300);

            }
        })
    };

    model.filter.subscribe(function(newValue) {
        model.resultPage(1);
    });

    model.filteredList = ko.computed(function() {
        var filter = model.filter();
        var page = model.resultPage();
        var pageSize = model.pageSize();
        var offset = (page - 1) * pageSize;
        var sortedList = _.sortBy(model.list(), function(g) { return g.name; });
        if (!filter) {
            var list = _.chain(sortedList).rest(offset).first(pageSize).value();
            model.listSize(sortedList.length);
            return list;
        }

        var filteredList = [];

        if (filter == "label:winners") {
            filteredList = ko.utils.arrayFilter(sortedList, function(item) {
                return item.bids && item.bids.length > 0;
            });
        } else {
            var re = new RegExp(filter,"gi");
            filteredList = ko.utils.arrayFilter(sortedList, function(item) {
                return (item.name + " " + item.number).match(re);
            });
        }
        var list = _.chain(filteredList).rest(offset).first(pageSize).value();
        model.listSize(filteredList.length);
        return list;
    });

    model.pageMin = ko.computed(function() {
        return (model.pageSize() * model.resultPage()) - model.pageSize() + 1
    });

    model.pageMax = ko.computed(function() {
        var c = (model.pageSize() * model.resultPage());
        var m = model.listSize();
        return m > c ? c : m;
    });

    eventHandlers.push(subscribe('/paddles/changed', function() {
        log("Trying to force paddle view render");
    }));

    eventHandlers.push(subscribe('/paddles/saved', function(guest, originator) {
        if (originator) {
            var f = model.form().reset();
            model.showingForm(false);
            publish("/ui/modal/end");
        }
        var id = null;
        var list = model.list();
        for(var i=0;i<list.length;i++) {
            if (guest.id == list[i].id) {
                id = i;
            }
        }

        if (guest.payments.length > 0) {
            var paymentsTotal = _.reduce(guest.payments, function(memo, p) {
                return memo + parseFloat(p.amount);
            }, 0);

            guest.totalPayments = paymentsTotal;
        } else {
            guest.totalPayments = 0;
        }

        if (id != null) {
            model.list.splice(id,1, guest);
        } else {
            model.list.push(guest);
        }
        if (originator) {
            publish("/ui/notifications/flash", ["Guest has been saved"]);
        }
    }));

    eventHandlers.push(subscribe('/paddles/save/error', function(paddle) {
        model.form().number.setError("Paddle number already in use by " + paddle.name);
    }));

    eventHandlers.push(
        subscribe('/paddles/removed', function(guestId, originator) {
            if (originator) {
                var f = model.form().reset();
                model.showingForm(false);
                publish('/ui/modal/end');
            }
            var id = null;
            var list = model.list();
            for(var i=0;i<list.length;i++) {
                if (guestId == list[i].id) {
                    id = i;
                    break;
                }
            }
            if (id != null) {
                model.list.splice(id,1);
            }
            if (originator) {
                publish("/ui/notifications/flash", ["Guest has been deleted"]);
            }
        })
    );

    eventHandlers.push(subscribe('/paddles/remove/error', function(message) {
        alert("You cannot remove a guest that has bid on items");
    }));

    eventHandlers.push(subscribe('/data/restored', function(data) {
        logger.debug("restoring paddle data");
        _.each(data.guests, function(g) {

            var paymentsTotal = _.reduce(g.payments, function(memo, p) {
                return memo + parseFloat(p.amount);
             }, 0);
            
            g.totalPayments = paymentsTotal;
        });
        model.list(data.guests);
        setTimeout(function() {
            publish('/paddles/loaded');
        }, 0);

    }));

    model.clearEventHandlers = function() {
        for(var i=0;i<eventHandlers.length;i++) {
            unsubscribe(eventHandlers[i]);
        }
    };

    return model;
};

models.paddles = new PaddleModel();

subscribe('/view/changed', function() {
    models.paddles.clearContent();
});

subscribe('/auth/destroy', function() {
    log("Clearing paddle data");
    models.paddles.clearEventHandlers();
    models.paddles = new PaddleModel();
});