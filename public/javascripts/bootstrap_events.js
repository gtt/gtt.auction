subscribe('/paddles/new', function() {
    setTimeout(function() {
        $("#newGuestModal").modal('show');
    }, 0);
});

subscribe('/paddles/saved', function(data, originator) {
    if (originator) {
        setTimeout(function() {
            $("#newGuestModal").modal('hide');
        }, 0);
    }
});

subscribe('/paddles/removed', function(data, originator) {
    if (originator) {
        setTimeout(function() {
            $("#newGuestModal").modal('hide');
        }, 0);
    }
});

subscribe('/items/new', function() {
    setTimeout(function() {
        $("#newItemModal").modal('show');
    }, 0);
});

subscribe('/items/saved', function(data, originator) {
    if (originator) {
        setTimeout(function() {
            $("#newItemModal").modal('hide');
        }, 0);
    }
});

subscribe('/items/removed', function(data, originator) {
    if (originator) {
        setTimeout(function() {
            $("#newItemModal").modal('hide');
        }, 0);
    }
});

subscribe('/bid/cleared', function(data, originator) {
    if (originator) {
        setTimeout(function() {
            $("#editBidModal").modal('hide');
        }, 0);
    }
});


subscribe('/view/changed', function(view) {
    $(".modal").on('shown.bs.modal', function() {
        $(this).find('input[type=text]:first').focus();
    });
});
