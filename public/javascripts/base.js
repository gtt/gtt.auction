//indexof mixin http://stackoverflow.com/a/12356923/185045
_.mixin({
    // return the index of the first array element passing a test
    indexOf: function(array, test) {
        // delegate to standard indexOf if the test isn't a function
        if (!_.isFunction(test)) return indexOfValue(array, test);
        // otherwise, look for the index
        for (var x = 0; x < array.length; x++) {
            if (test(array[x])) return x;
        }
        // not found, return fail value
        return -1;
    }
});

// hide labels if placeholder available
$(function() {
    if (Modernizr.input.placeholder) {
        $("body").addClass("has-placeholder");
    }
});

// listen for changes to the url (requires HTML5 - IE8+)
window.onhashchange = function(e,b) {
    var newUrl = e.newURL;
    var oldUrl = e.oldURL;
    var url = window.location.href;
    log("Current URL=", url);
    var view = url.split('#')[1];

    logger.debug("Switching view = ", view);
    publish('/view/change', [view]);

};

window.onbeforeunload = function (e) {
    logger.debug("Back button pressed");
    publish('/ui/navigation/back');
}

// basic app object
var app = function() {
    var a = {
        pageCache: {},
        auth_code: null,
        activeView: null,
        initialView: "login",
        ready: false,
        modal_active: false
    };

    a.createFakeData = function() {
        console.log("Load fake data");
        publish('/data/fake');
    };

    a.changeView = function(view) {
        log("changing view", view);

        if (!a.auth_code && view != "signout") {
            view = "login";
            window.location.hash = "login";
            log("Not signed in - routing to login");
        }

        if (view == "signout") {
            logger.info("Requesting signout")
            publish('/auth/destroy');
            return;
        }
        if (view != a.activeView) {
            if (a.pageCache[view]) {
                $("#content").html(a.pageCache[view]);
                publish('/view/changed', [view]);

            } else {
                $.get('/partials/' + view, function(data) {
                    a.pageCache[view] = data;
                    $("#content").html(a.pageCache[view]);
                    publish('/view/changed', [view]);
                });
            }
        }
        a.activeView = view;
    }

    if (window.location.hash) {
        log("Extracting hash", window.location.hash);
        a.initialView = window.location.hash.split("#")[1];
    }
    a.changeView(a.initialView);
    return a;
}();

subscribe('/auth/successful', function(code) {
    log("authentication successful", code);
    app.auth_code = code;
    publish("/data/resync");
    if (app.initialView == "login") {
        app.initialView = "home";
    }
    log("Attempting to restore view", app.initialView);
    window.location.hash = app.initialView;
    publish("/view/change", [app.initialView]);
});

subscribe('/auth/destroy', function() {
    app.auth_code = null;
    window.location.hash = "login";
    publish("/view/change", ["login"]);
});

subscribe('/view/navigate', function(view) {
    window.location.hash = view;
});

// handle view changes
subscribe('/view/change', function(view) {
    app.changeView(view);
});

subscribe('/view/changed', function(view) {
    var element = $("#content div")[0];
    log("Applying View", view);
    if (models[view]) {
        ko.applyBindings(models[view], element);
        if (!app.ready) {
            log("app ready");
            publish('/app/ready');
            app.ready = true;
        }
    }
    $("nav a").removeClass("active");
    $("#menu_" + view).addClass("active");
    $("input[type=text]:visible:first").focus();
});

subscribe('/data/resync', function() {
    if (app.auth_code) {
        publish('/data/restore', [app.auth_code]);
    }
});

subscribe('/data/restored', function(data) {
    log("Restoring data", data);
});

subscribe('/auth/new', function() {
    var view = "login";
    window.location.hash = "login";
    publish('/view/change', [view]);
});

subscribe('/remote/disconnected', function() {
    $("#disconnected").show();
});

subscribe("/remote/connecting", function() {
    $("#preConnectedContent .loading-text div").text("Connecting...");
});

subscribe('/remote/connected', function() {
    $("#disconnected").hide();
});

subscribe('/cursor/search', function() {
    log("finding field to search");
    $("input[type=text]:visible:first").focus().select();
});

subscribe('/items/print', function(button) {
    var form = $(button.currentTarget).parent();
    $(form).submit();
    setTimeout(function() {
        publish('/items/printed');
    }, 200);

});

subscribe('/initialize', function() {
   setTimeout(function() {
       $("#preConnectedContent").hide(0, function() {
           $("#connectedContent").fadeIn();
       });
   }, 1000);
});

subscribe('/ui/waiting/start', function() {
    logger.debug("starting waiting");
    $(".waiting").show();
    $(".waiting-cover").show();
});

subscribe('/ui/waiting/end', function() {
    logger.debug("ending waiting");
    $(".waiting").hide();
    $(".waiting-cover").hide();
});

subscribe('/ui/modal/start', function() {
    logger.debug("starting modal - hiding nav");
    var h = $("nav").height();
    $("nav").fadeOut(function() {
        $("body").css("padding-top", (h + 4) + "px");
    });
    app.modal_active = true;
    publish('/cursor/search');
});

subscribe('/ui/modal/end', function() {
    logger.debug("ending modal - showing nav");
    $("body").css("padding-top", "0px");
    $("nav").show();
    $(".more-content").hide();
    $('.more-switch button i').removeClass("chevron-rotated");
    app.modal_active = false;
    window.scrollTo(0, 0);
});

$(function() {
    // onstart events here
    $("#content").on("click", ".more-switch", function() {
        logger.debug("showing more");
        $(".more-content").slideToggle();
        $('.more-switch button i').toggleClass("chevron-rotated");
    });
});
