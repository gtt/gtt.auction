key('ctrl+1', function() {
    publish('/view/navigate', ['home']);
    return false;
});

key('ctrl+2', function() {
    publish('/view/navigate', ['paddles']);
    return false;
});

key('ctrl+3', function() {
    publish('/view/navigate', ['items']);
    return false;
});

key('ctrl+4', function() {
    publish('/view/navigate', ['bidding']);
    return false;
});

key('ctrl+5', function() {
    publish('/view/navigate', ['checkout']);
    return false;
});

key('ctrl+6', function() {
    publish('/view/navigate', ['reports']);
    return false;
});

key('ctrl+/', function() {
    publish('/cursor/search');
    return false;
});

key('up', "bidding", function() {

});

key('down', "bidding", function() {

});

key('up', "paddles", function() {

});

key('down', "paddles", function() {

});

subscribe('/view/changed', function(view) {
    key.setScope(view);
});

key.filter = function(event) {
    return true;
};