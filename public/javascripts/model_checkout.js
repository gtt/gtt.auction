var models = models || {};

var CheckoutModel = function() {

    var self = this;
    var eventHandlers = [];

    var model = {
        filter: ko.observable(""),
        active: false,
        guestList: ko.observableArray([]),
        paymentList: ko.observableArray([]),
        saving: ko.observable(false),

        selectGuest: function(guest) {
            model.form().guestName(guest.name + " (" + guest.number + ")");
            model.form().guest = guest;
            model.form().bidList(guest.bids || []);
            model.form().expectedPayment(guest.expectedPayment);
            model.form().paymentAmount(guest.totalPayments);
            return false;
        },

        selectCombineGuest: function(guest) {
            model.form().combineGuestName(guest.name + " (" + guest.number + ")");
            model.form().combineGuest = guest;
            return false;
        },

        deletePayment: function(payment) {
            log("Delete Payment", payment);
            if (!payment.paddle && !payment.guest) {
                alert("Payee information is missing");
            }

            if (confirm("Delete payment of " + payment.amount +"?")) {
                var paddle = payment.paddle || payment.guest.id;
                publish("/checkout/delete", [{paddle: paddle, id: payment.id}]);
            }
        },

        form: ko.validatedObservable({
            guestLookup: ko.observable(""),
            guestList: ko.observableArray([]),
            guestName: ko.observable(""),
            guest: null,
            bidList: ko.observableArray([]),
            expectedPayment: ko.observable(""),
            paymentAmount: ko.observable(""),

            combinePaddle: ko.observable(false),

            combineGuestLookup: ko.observable(""),
            combineGuestList: ko.observableArray([]),
            combineGuestName: ko.observable(""),
            combineGuest: null,

            amount: ko.observable().extend({ number: true, required: { params: true, message: "Required" } }),
            paymentMethod: ko.observable("").extend({ required: { params: true, message: "Required" } }),
            checkNumber: ko.observable(""),
            note: ko.observable(""),

            combineGuests: function() {
                var g = model.form().guest;
                var c = model.form().combineGuest;
                if (g == null || c == null) {
                    alert("please provide two guests to combine");
                    return;
                }
                if (g == c) {
                    alert("Guests are the same please select two different guests");
                    return;
                }

                if (!c.bids || c.bids.length == 0) {
                    alert(c.name + " has no items to transfer");
                    return;
                }

                if (confirm("All " + c.name + " winning bids will be transferred to " + g.name + ". Is this correct?")) {

                    var data = _.map(c.bids, function(item) { return item.id });

                    publish("/bids/combine", [{ combineGuest: g.id, items: data }]);
                }
            },
            clearGuest: function() {
                var g = model.form().guest;
                model.form().guest = null;
                model.form().bidList([]);
                model.form().guestName('');
                model.form().guestLookup('');
                model.form().expectedPayment("");
                model.form().paymentAmount("");
                model.form().clearCombineGuest();
                model.form().combinePaddle(false);
            },
            clearCombineGuest: function() {
                var g = model.form().guest;
                model.form().combineGuest = null;
                model.form().combineGuestName('');
                model.form().combineGuestLookup('');
            },
            closeCombineGuest: function() {
                model.form().clearCombineGuest();
                model.form().combinePaddle(false);
            },
            clearForm: function() {
                    model.form().clearGuest();
                    model.form().amount("");
                    model.form().amount.isModified(false);
                    model.form().paymentMethod("");
                    model.form().paymentMethod.isModified(false);
                    model.form().checkNumber("");
                    model.form().checkNumber.isModified(false);
                    model.form().note("");
                    model.form().note.isModified(false);
            },
            startCombine: function() {
              model.form().combinePaddle(true);
            },
            save: function() {
                if (!model.form().guest) {
                    model.form().guestName.setError("Please select a guest");
                }
                if (model.form().paymentMethod() == "check" && !model.form().checkNumber()) {
                    model.form().checkNumber.setError("Required");
                } else {
                    model.form().checkNumber.clearError();
                }
                var valid = model.form.isValid();

                if (valid) {
                    publish('/checkout/save', [
                        {
                            paddle: model.form().guest.id,
                            amount: model.form().amount(),
                            checkNumber: model.form().checkNumber(),
                            note: model.form().note(),
                            method: model.form().paymentMethod()
                        }
                    ]);
                    model.saving(true);
                } else {
                    model.form.errors.showAllMessages();
                }
            }
        })
    };

    model.form().outstandingAmount = ko.computed(function() {
        var amt = model.form().expectedPayment() - model.form().paymentAmount();
        return amt > 0 ? amt : 0;
    });

    model.filteredList = ko.computed(function() {
        var filter = model.filter();
        var list = model.guestList();

        var filteredList = ko.utils.arrayFilter(list, function(guest) {
            return guest.totalPayments > 0;
        });

        return filteredList;
    });

    model.clearEventHandlers = function() {
        for(var i=0;i<eventHandlers.length;i++) {
            unsubscribe(eventHandlers[i]);
        }
    };

    model.form().paymentMethod.subscribe(function(val) {
        if (val != "check") {
            model.form().checkNumber("");
            model.form().checkNumber.isModified(false);
        }
    });

    var guestLookup = function(val, resultsCallback) {
        if (!val) {
            resultsCallback([]);
            return;
        }
        var guests = models.paddles.list();
        var re = new RegExp(val,"gi");
        var direct = null;
        var count = 0;
        var filteredList = ko.utils.arrayFilter(guests, function(item) {
            if (item.number == val) {
                direct = item;
                count++;
                return false;
            }
            if (count == 5 || item.bid) {
                return false;
            }
            var m = (item.name + " " + item.number).match(re);
            if (m) {
                count++;
            }
            return m;
        });

        if (direct) {
            filteredList.unshift(direct);
        }
        resultsCallback(filteredList);
    };

    model.form().combineGuestLookup.subscribe(function(val) {
       guestLookup(val, function(results) {
          model.form().combineGuestList(results);
       });
    });

    model.form().guestLookup.subscribe(function(val) {
        if (!val) {
            model.form().guestList([]);
            return;
        }
        var guests = models.paddles.list();
        var re = new RegExp(val,"gi");
        var direct = null;
        var count = 0;
        var filteredList = ko.utils.arrayFilter(guests, function(item) {
            if (item.number == val) {
                direct = item;
                count++;
                return false;
            }
            if (count == 5 || item.bid) {
                return false;
            }
            var m = (item.name + " " + item.number).match(re);
            if (m) {
                count++;
            }
            return m;
        });

        if (direct) {
            filteredList.unshift(direct);
        }
        model.form().guestList(filteredList);
    });

    eventHandlers.push(subscribe("/bids/combined", function(items) {
        var itemList = models.items.list();
        _.each(items, function(newItem, index) {
            for(var i=0;i<itemList.length;i++) {
                var oldItem = itemList[i];
                if (newItem.id == oldItem.id) {
                    models.items.list.splice(i,1, newItem);
                }
            }
        });
        publish("/bid/changed");
        model.form().closeCombineGuest();
    }));
    eventHandlers.push(subscribe("/bid/refreshed", function(items) {
        if (model.form().guest) {
            var g = model.form().guest;
            model.form().expectedPayment(g.expectedPayment);
            model.form().paymentAmount(g.totalPayments);
            model.form().bidList(g.bids);
        }
    }));

    eventHandlers.push(subscribe('/checkout/deleted', function(paymentDelete) {
        var guest = _.find(models.paddles.list(), function(p) { return p.id == paymentDelete.paddle });
        if (guest && guest.payments) {
            var deleteId = null;
            for(var i=0;i<guest.payments.length;i++) {
                var p = guest.payments[i];
                if (p.id == paymentDelete.id) {
                    deleteId = i;
                    p.guest = null;
                    break;
                }
            }

            var paymentsTotal = guest.totalPayments;
            if (deleteId >= 0) {
               var payments = guest.payments.splice(deleteId, 1);
               paymentsTotal = paymentsTotal - payments[0].amount;
            }

            var listId = null;
            var list = model.paymentList();
            for(var i=0;i<list.length;i++) {
                var p = list[i];
                if (p.id == paymentDelete.id) {
                    listId = i;
                }
            }
            if (listId != null) {
                model.paymentList.splice(listId, 1);
            }

            if (model.form().guest != null && model.form().guest.id == paymentDelete.paddle) {
                model.form().guest = guest;
                model.form().paymentAmount(paymentsTotal);
            }

            guest.totalPayments = paymentsTotal;
            publish("/paddles/saved", [guest, false]);
        }
    }));

    eventHandlers.push(subscribe('/checkout/saved', function(payment, originator) {
        log("Checkout save successful");

        var guest = _.find(models.paddles.list(), function(p) { return p.id == payment.paddle });
        if (guest) {
            guest.payments.push(payment);
            payment.guest = guest;
            model.paymentList.push(payment);
            model.form().clearForm();

            var paymentsTotal = _.reduce(guest.payments, function(memo, p) {
               return memo + parseFloat(p.amount);
            }, 0);

            guest.totalPayments = paymentsTotal;
            if (originator) {
                model.saving(false);
            }
            publish("/paddles/saved", [guest, false]);
        }
    }));

    eventHandlers.push(subscribe("/bid/changed", function() {
        log("refreshing bid list");
        var guestList = models.paddles.list();
        var itemList = models.items.list();
        var paymentList = [];
        model.guestList(guestList);

        _.each(guestList, function(g) {
            g.bids = [];
            g.expectedPayment = 0;
            g.payments = g.payments || [];
            if (g.payments.length > 1) {
                var sum = 0;
                for(var i = 0;i < g.payments.length;i++) {
                    var p = g.payments[i];
                    p.guest = g;
                    sum = sum + parseFloat(p.amount);
                    paymentList.push(p);
                    g.totalPayments = sum;
                }
            } else if (g.payments.length == 1){
                var payment = g.payments[0];
                payment.guest = g;
                g.totalPayments = parseFloat(payment.amount);
                paymentList.push(payment);
            } else{
                g.totalPayments = 0;
            }
        });

        model.paymentList(paymentList);

        var guests = _.object(_.map(guestList, function(item) {
            return [item.id, item];
        }));

        _.each(itemList, function(item) {
            if (item.bid) {
                var guest = guests[item.bid.paddle];
                guest.bids.push(item);
                guest.expectedPayment = guest.expectedPayment + parseFloat(item.bid.amount);
            }
        });
        publish("/paddles/changed");
        publish("/bid/refreshed");
    }));

    return model;
};

models.checkout = new CheckoutModel();

subscribe('/view/changed', function(view) {
    if (view == "checkout") {
        models.checkout.active = true;
    }
});

subscribe('/auth/destroy', function() {
    log("Clearing checkout data");
    models.checkout.clearEventHandlers();
    models.checkout = new CheckoutModel();
});
