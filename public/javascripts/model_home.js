var models = models || {};

var HomeModel = function() {
    var model = {
        itemCount: ko.computed(function() {
            return models.items.list().length;
        }),
        guestCount: ko.computed(function() {
            return models.paddles.list().length;
        }),
        bidCount: ko.computed(function() {
            return _.filter(models.items.list(), function(item) { return item.bid; }).length;
        }),
        bidAmount: ko.computed(function() {
            var bids = _.filter(models.items.list(), function(item) { return item.bid });
            var amt = _.reduce(bids, function(memo, item) { return memo + parseFloat(item.bid.amount) }, 0);
            return accounting.formatMoney(amt);
        }),
        amountRaised: ko.computed(function() {
            var payments = models.checkout.paymentList();
            var amt = _.reduce(payments, function(memo, p) { return memo + parseFloat(p.amount) }, 0);
            return accounting.formatMoney(amt);
        })
    }
    return model;
};

subscribe('/auth/destroy', function() {
    models.home = new HomeModel();
});

subscribe('/auth/successful', function() {
    models.home = new HomeModel();
});