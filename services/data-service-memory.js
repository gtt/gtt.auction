module.exports = function(callback) {
    console.log("Starting memory data storage");

    var auctions = {};

    callback({
        add: function(auction, cb) {
            console.log("Memory Add");
            auctions[auction._id] = auction;
            cb(null, auction);
        },
        load: function(id, cb) {
            console.log("Memory Load Auction");
            if (auctions[id]) {
                cb(null, auctions[id]);
            } else {
                cb(null, null);
            }
        },
        save: function(auction, cb) {
            console.log("Memory Save");
            auction._id = auction.code;
            auctions[auction.code] = auction;
            cb(null, auction);
        }
    });
};