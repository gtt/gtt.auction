'use strict';

var MongoClient = require('mongodb').MongoClient;
var config = require('../config');
var fs = require('fs');
var path = require('path');

module.exports = function(callback) {
    var uri = config.mongo.uri;
    MongoClient.connect(uri, { auto_reconnect: true, poolSize: 10 }, function(errors, client) {
        if (!errors) {
            console.log("Connected to MongoDB");
            client.on("close", function() {
                console.log("Closing Connection");
            });
            var db = client.db('auction');
            callback({
                add: function(auction, cb) {
                    db.collection("auctions").insert(auction, { safe: true }, function(errors, result) {
                        cb(errors, result);
                    });
                },
                load: function(code, cb) {
                    db.collection("auctions").findOne({ _id: code }, function(errors, result) {
                        cb(errors, result);
                    });
                },
                save: function(auction, cb) {
                    db.collection("auctions").save(auction, { safe: true }, function(errors, result) {
                        if (errors) {
                            console.log("error saving data");
                            cb(errors, null);
                        } else {
                            console.log("data saved");
                            cb(null, result);
                        }
                    });
//                    try {
//                        var p = path.join(__dirname, 'data', auction._id + ".json");
//                        console.log(p);
//                        fs.writeFile(p, JSON.stringify(auction), function (err) {
//                            if (err) return console.log(err);
//                            console.log("File Saved");
//                        });
//                    } catch(ex) {
//                        console.log("Error saving file", ex);
//                    }

                }
            });

        } else {
            console.log("Error connecting to DB", errors);
            db.close();
            console.log(errors);
            callback("Error connecting to MongoDB");
        }
    });
};